/**
 * Class for wrapping RestResponse objects to ease up the handling of them by using methods like getJSONBody and addHeader
 */
public with sharing class ARC_WebserviceResponse
{
    public static final String HEADER_CONTENTTYPE = 'Content-Type';

    public static final String HEADER_DURATION = 'Webservice-Duration';

    public static final String HEADER_CORS = 'Access-Control-Allow-Origin';

    public final Map<String, String> headers = new Map<String, String>();

    public Object bodyObject { get; set; }

    public Integer statusCode { get; set; }

    public double duration { get; set; }


    /**
     * Returns the JSON string of the response
     * Can be used for unifying the response body e.g. by adding general attributes to all response JSONs, for instance duration
     */
    public String getJSONBody()
    {
        // General information could be added to the response body here, e.g. duration
        return JSON.serialize(this.bodyObject);
    }
}