/**
 * Controller class for handling REST requests retrieved by ARC_RestServiceEndpoint
 */
public with sharing class ARC_RestServiceController implements ARC_WebserviceController
{
    public ARC_WebserviceRequest webserviceRequest { get; private set; }

    public ARC_WebserviceResponse webserviceResponse { get; private set; }

    public Object webservicePayload { get; private set; }

    private ARC_WebserviceAdapterFactory adapterFactory;

    private RestRequest restRequest;

    private RestResponse restResponse;

    private ARC_Service_Error errorLogger;


    public ARC_RestServiceController (
        RestRequest restRequest,
        RestResponse restResponse,
        ARC_WebserviceAdapterFactory adapterFactory,
        ARC_Service_Error errorLogger
    )
    {
        this.restRequest = restRequest;
        this.restResponse = restResponse;
        this.adapterFactory = adapterFactory;
        this.errorLogger = errorLogger;
    }


    /**
     * TODO Move endpoint related stuff to Endpoint and routing related to dispatching
     */
    public void execute ()
    {
        this.webserviceRequest = this.createRequest();
        this.webserviceResponse = this.createResponse();

        System.Savepoint dbSavePoint = null;
        ARC_WebserviceRoute route = null;

        try {

            route = this.adapterFactory.getRoute(
                webserviceRequest.requestUri,
                webserviceRequest.serviceVersion,
                webserviceRequest.httpMethod
            );

            if (route == null) {

                webserviceResponse.statusCode = 404;
                webserviceResponse.bodyObject = new ARC_DTO_WebserviceError(
                    'NOT_FOUND',
                    'No adapter class found for requested service.'
                );

            } else {

                webserviceRequest.pathParameters.putAll(route.getPathVariables());

                ARC_WebserviceAdapter adapter = route.getAdapter();
                webservicePayload = this.createPayloadObject(adapter, webserviceRequest);

                dbSavePoint = Database.setSavepoint();

                adapter.execute(webserviceRequest, webserviceResponse, webservicePayload);
            }

        } catch (Exception e) {

            if (route != null && dbSavePoint != null) {
                Database.rollback(dbSavePoint);
                dbSavePoint = null;
                route = null;
            }

            webserviceResponse.statusCode = 500;
            webserviceResponse.bodyObject = new ARC_DTO_WebserviceError(
                'An unexpected exception occurred.',
                'Exception:' + e.getTypeName()
                    + '\nMessage: ' + e.getMessage()
            );

            errorLogger.log(ARC_Error.Scope.REST, ARC_Error.Code.E6100, e);
        }

        this.restResponse.statusCode = webserviceResponse.statusCode;
        this.restResponse.responseBody = Blob.valueOf(webserviceResponse.getJSONBody());

        webserviceResponse.duration = (DateTime.now().getTime() - webserviceRequest.requestTime.getTime()) / 1000.0;
        webserviceResponse.headers.put(ARC_WebserviceResponse.HEADER_DURATION, String.valueOf(webserviceResponse.duration));

        // setting headers from WSA response
        for (String key : webserviceResponse.headers.keySet()) {
            this.restResponse.addHeader(key, webserviceResponse.headers.get(key));
        }

        // disabling caching on all responses
        this.restResponse.addHeader('Cache-Control', 'no-cache,s-maxage=0');
        this.restResponse.addHeader('Expires', '-1');
    }


    private Object createPayloadObject (ARC_WebserviceAdapter adapter, ARC_WebserviceRequest request)
    {
        if (!request.hasBody()) {
            return null;
        }

        System.Type payloadType = adapter.getRequestPayloadType();

        if (request.isJSON()) {
            if (payloadType == null) {
                return JSON.deserializeUntyped(request.requestBody.toString());
            }
            return JSON.deserializeStrict(request.requestBody.toString(), payloadType);
        }

        if (payloadType != null) {
            throw new JSONRequestBodyRequiredException();
        }

        return request.requestBody;
    }


    /**
     * TODO Move to endpoint
     */
    private ARC_WebserviceRequest createRequest ()
    {
        // clean requestUri from endpoint settings
        String resourcePath = this.restRequest.resourcePath.replace('/services/apexrest', '').replace('/*', '');
        String requestUri = this.restRequest.requestURI.replace(resourcePath, '');

        List<String> uriParts = requestUri.split('/');

        // leading slash is uncluded in requestUri
        String serviceVersion = uriParts.get(1);
        String requestPath = requestUri.replace('/' + serviceVersion, '');

        ARC_WebserviceRequest request = new ARC_WebserviceRequest(requestPath, serviceVersion);

        request.httpMethod = this.restRequest.httpMethod;
        request.headers.putAll(this.restRequest.headers);
        request.requestURI = requestPath;
        request.urlParameters.putAll(this.restRequest.params);
        request.requestBody = this.restRequest.requestBody;

        return request;
    }


    /**
     * TODO Move to endpoint/future dispatcher
     */
    private ARC_WebserviceResponse createResponse ()
    {
        ARC_WebserviceResponse webserviceResponse = new ARC_WebserviceResponse();
        webserviceResponse.statusCode = 200;
        webserviceResponse.headers.put(ARC_WebserviceResponse.HEADER_CONTENTTYPE, 'application/json');
        webserviceResponse.headers.put(ARC_WebserviceResponse.HEADER_DURATION, String.valueOf(webserviceResponse.duration));
        webserviceResponse.headers.put(ARC_WebserviceResponse.HEADER_CORS, '*');

        return webserviceResponse;
    }



    public class JSONRequestBodyRequiredException extends Exception
    {
    }
}