@isTest
public class ARC_WebserviceRequestTest {

    @isTest
    public static void testObjectCreation()
    {
        Test.startTest();

        ARC_WebserviceRequest request = new ARC_WebserviceRequest('name', 'v42');

        request.requestUri = '/click/a/b/c';
        request.requestBody = Blob.valueOf('Request Body!');

        Test.stopTest();

        System.assertEquals('name', request.serviceName);
        System.assertEquals('v42', request.serviceVersion);
        System.assertEquals('/click/a/b/c', request.requestUri);
        String dateTimeFormat = 'yyyy-MM-dd HH:mm';
        System.assertEquals(Datetime.now().format(dateTimeFormat), request.requestTime.format(dateTimeFormat));
        System.assertEquals(0, request.headers.size());
        System.assertEquals(0, request.urlParameters.size());
        System.assertEquals(0, request.pathParameters.size());
        System.assertEquals('Request Body!', request.requestBody.toString());
    }


    @isTest
    public static void testIsJSONTrue()
    {
        // check for application/json contenttype
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        request.headers.put('Content-Type', 'application/json; charset=UTF-8');

        Test.startTest();

        System.assert(request.isJSON());

        Test.stopTest();
    }


    @isTest
    public static void testIsJSONFalse()
    {
        // check for application/json contenttype
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        Test.startTest();

        System.assert(!request.isJSON());

        Test.stopTest();
    }


    @isTest
    public static void testHasBodyFalse()
    {
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        request.httpMethod = 'GET';

        Test.startTest();
        System.assert(!request.hasBody());
        Test.stopTest();
    }


    @isTest
    public static void testHasBodyTrueForPatch()
    {
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        request.httpMethod = 'PATCH';

        Test.startTest();
        System.assert(request.hasBody());
        Test.stopTest();
    }


    @isTest
    public static void testHasBodyTrueForPost()
    {
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        request.httpMethod = 'POST';

        Test.startTest();
        System.assert(request.hasBody());
        Test.stopTest();
    }


    @isTest
    public static void testHasBodyTrueForPut()
    {
        ARC_WebserviceRequest request = new ARC_WebserviceRequest('', 'v0');

        request.httpMethod = 'PUT';

        Test.startTest();
        System.assert(request.hasBody());
        Test.stopTest();
    }
}