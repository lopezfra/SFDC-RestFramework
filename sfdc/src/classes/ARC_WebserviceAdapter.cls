/**
 * Webservice adapter interface. Each webservice adapter has to implement this interface
 */
public interface ARC_WebserviceAdapter
{
    /**
     * Main adapter logic
     */
    void execute (ARC_WebserviceRequest request, ARC_WebserviceResponse response, Object payload);


    /**
     * Returns the Type of the defined paylaod DTO class
     */
    System.Type getRequestPayloadType ();
}