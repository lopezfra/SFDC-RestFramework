/**
 * Implementation of ARC_WebserviceAdapterFactory
 * Creates ARC_WerbserviceAdapter instances based on registered adapter classes
 *
 * TODO Move routing to dispatcher
 */
public interface ARC_WebserviceAdapterFactory
{
    /**
     * @param requestPath Path to get route for
     * @param version Version to get route for
     * @param httpMethod HttpMethod to get route for
     * @return Route if found; null ifnot
     */
    ARC_WebserviceRoute getRoute (String requestPath, String version, String httpMethod);
}