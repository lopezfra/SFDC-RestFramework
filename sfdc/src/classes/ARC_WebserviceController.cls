/**
 * Controller interface for handling REST requests retrieved by ARC_RestServiceEndpoint
 */
public interface ARC_WebserviceController
{
    /**
     * Method for handling HTTP requests
     */
    void execute ();
}