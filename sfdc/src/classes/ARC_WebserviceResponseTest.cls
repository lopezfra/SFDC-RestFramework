@isTest
public class ARC_WebserviceResponseTest
{
    @isTest
    public static void testStatusCodeAndDuration()
    {
        Test.startTest();
        ARC_WebserviceResponse response = new ARC_WebserviceResponse();
        response.statusCode = 404;
        response.duration = 0.666;
        System.assertEquals(404, response.statusCode);
        System.assertEquals(0.666, response.duration);
        Test.stopTest();
    }

    @isTest
    public static void testGetJSONBody()
    {
        Map<String,Object> data = new Map<String,Object>();
        data.put('a',1);
        data.put('b','2');

        Test.startTest();

        ARC_WebserviceResponse response = new ARC_WebserviceResponse();
        response.bodyObject = data;
        String jsonStr = response.getJSONBody();

        Test.stopTest();

        Map<String,Object> r = (Map<String,Object>)JSON.deserializeUntyped(jsonStr);
        System.assertEquals(2, r.size());
        System.assertEquals(1, r.get('a'));
        System.assertEquals('2', r.get('b'));
    }

}