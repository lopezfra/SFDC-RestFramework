/**
 * Rest based version of this route
 */
public with sharing class ARC_RestWebServiceRoute implements ARC_WebserviceRoute
{
    private static final Pattern ROUTE_PATH_PATTERN = Pattern.compile(':([^/]+)');

    private ARC_WebserviceAdapter adapter;

    private System.Type adapterType;

    private String httpMethod;

    private String version;

    private String path;

    private final Map<String, String> pathVariables = new Map<String, String>();


    public ARC_RestWebServiceRoute (System.Type adapterType, String version, String path, String httpMethod)
    {
        this.adapterType = adapterType;
        this.version = version;
        this.path = path;
        this.httpMethod = httpMethod;

        this.detectPathVariables(this.path);
    }


    public ARC_WebserviceAdapter getAdapter ()
    {
        return this.adapter;
    }


    public void setAdapter (ARC_WebserviceAdapter adapter)
    {
        this.adapter = adapter;
    }


    public System.Type getAdapterType ()
    {
        return this.adapterType;
    }


    public String getHttpMethod ()
    {
        return this.httpMethod;
    }


    public String getVersion ()
    {
        return this.version;
    }


    public String getPath ()
    {
        return this.path;
    }


    public Map<String, String> getPathVariables ()
    {
        return this.pathVariables;
    }


    private void detectPathVariables (String path)
    {
        // retrieve names of pathVariables
        Matcher routeMatcher = ROUTE_PATH_PATTERN.matcher(path);

        while (routeMatcher.find()) {
            this.pathVariables.put(routeMatcher.group(1), null);
        }
    }
}