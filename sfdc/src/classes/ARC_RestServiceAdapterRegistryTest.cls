/**
 * Test for the rest service adapter factory
 */
@IsTest
private with sharing class ARC_RestServiceAdapterRegistryTest
{
    private static ARC_RestServiceAdapterRegistry registry;


    private static void setUp ()
    {
        fflib_ApexMocks mocks = new fflib_ApexMocks();

        registry = new ARC_RestServiceAdapterRegistry();
    }


    @IsTest
    private static void testGetAdapterReturnsNullIfNoAdapterRegisteredAtAll ()
    {
        setUp();

        Test.startTest();

        System.assertEquals(null, registry.getRoute('/path', 'v1', 'GET'));

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsNullIfPathNotFound ()
    {
        setUp();

        registry.addRoute('/path', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(null, registry.getRoute('/another-path', 'v1', 'GET'));

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsNullIfVersionNotFound ()
    {
        setUp();

        registry.addRoute('path', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(null, registry.getRoute('/path', 'v2', 'GET'));

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsNullIfMethodNotFound ()
    {
        setUp();

        registry.addRoute('/path', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(null, registry.getRoute('path', 'v1', 'POST'));

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsAdapterForDirectPathMatch ()
    {
        setUp();

        registry.addRoute('/path', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(ARC_WebserviceAdapter.class, registry.getRoute('/path', 'v1', 'GET').getAdapterType());

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsAdapterForDirectNestedPathMatch ()
    {
        setUp();

        registry.addRoute('/path1/path2', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(ARC_WebserviceAdapter.class, registry.getRoute('/path1/path2', 'v1', 'GET').getAdapterType());

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsAdapterForPathWithTrailingVariables ()
    {
        setUp();

        registry.addRoute('/path1/path2', 'v1', 'GET', TestAdapter1.class);
        registry.addRoute('/path1/:var1', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(ARC_WebserviceAdapter.class, registry.getRoute('/path1/value1', 'v1', 'GET').getAdapterType());

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsAdapterForPathWithLeadingVariables ()
    {
        setUp();

        registry.addRoute('/path1/path2', 'v1', 'GET', TestAdapter1.class);
        registry.addRoute('/:var1/path1', 'v1', 'GET', ARC_WebserviceAdapter.class);

        Test.startTest();

        System.assertEquals(ARC_WebserviceAdapter.class, registry.getRoute('/value1/path1', 'v1', 'GET').getAdapterType());

        Test.stopTest();
    }


    @IsTest
    private static void testGetAdapterReturnsAdapterForMultipleRegisterdPaths ()
    {
        setUp();

        registry.addRoute('/path1', 'v1', 'GET', ARC_WebserviceAdapter.class);
        registry.addRoute('/path1/path2', 'v1', 'GET', TestAdapter1.class);
        registry.addRoute('/part1/:var1/part2/:var2', 'v1', 'GET', TestAdapter2.class);

        Test.startTest();

        System.assertEquals(null, registry.getRoute('/path1/part2/value1', 'v1', 'GET'));
        System.assertEquals(null, registry.getRoute('/part1/part2/value1/value2', 'v1', 'POST'));
        System.assertEquals(TestAdapter2.class, registry.getRoute('/part1/value1/part2/value2', 'v1', 'GET').getAdapterType());
        System.assertEquals(TestAdapter1.class, registry.getRoute('/path1/path2', 'v1', 'GET').getAdapterType());
        System.assertEquals(ARC_WebserviceAdapter.class, registry.getRoute('/path1', 'v1', 'GET').getAdapterType());

        Test.stopTest();
    }


    private class TestAdapter1 implements ARC_WebserviceAdapter
    {
        public void execute (ARC_WebserviceRequest request, ARC_WebserviceResponse response, Object payload)
        {}

        public System.Type getRequestPayloadType()
        {
            return null;
        }
    }


    private class TestAdapter2 implements ARC_WebserviceAdapter
    {
        public void execute (ARC_WebserviceRequest request, ARC_WebserviceResponse response, Object payload)
        {}

        public System.Type getRequestPayloadType()
        {
            return null;
        }
    }
}