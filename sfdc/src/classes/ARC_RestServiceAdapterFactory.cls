/**
 * Creates ARC_WerbserviceAdapter instances based on registered adapter classes
 */
public with sharing class ARC_RestServiceAdapterFactory implements ARC_WebserviceAdapterFactory
{
    private ARC_WebserviceAdapterRegistry registry;


    public ARC_RestServiceAdapterFactory (ARC_WebserviceAdapterRegistry registry)
    {
        this.registry = registry;
    }


    public ARC_WebserviceRoute getRoute (String requestPath, String version, String httpMethod)
    {
        ARC_WebserviceRoute route = this.registry.getRoute(requestPath, version, httpMethod);

        if (route == null) {
            return null;
        }

        System.Type klass = route.getAdapterType();

        Object adapterImplementation = klass.newInstance();
        ARC_WebserviceAdapter adapter = (ARC_WebserviceAdapter) adapterImplementation;

        route.setAdapter(adapter);

        return route;
    }
}