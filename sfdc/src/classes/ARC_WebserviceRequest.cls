/**
 * Class for wrapping RestReqeust objects to ease up the handling of them by using methods like isJSON and hasBody
 */
public with sharing class ARC_WebserviceRequest
{
    public final Map<String, String> headers = new Map<String, String>();

    public final Map<String, String> urlParameters = new Map<String, String>();

    public final Map<String, String> pathParameters = new Map<String, String>();

    public Blob requestBody { get; set; }

    public String serviceName { get; set; }

    public String serviceVersion { get; set; }

    public DateTime requestTime { get; private set; }

    public String httpMethod { get; set; }

    public String requestUri { get; set; }


    public ARC_WebserviceRequest(
        String serviceName,
        String serviceVersion
    ) {
        this.serviceName = serviceName;
        this.serviceVersion = serviceVersion;
        this.requestTime = DateTime.now();
    }


    /**
     * Checks if the defined rest header content type is set to 'application/json'
     */
    public Boolean isJSON()
    {
        String contentType = this.headers.get('Content-Type');
        if (contentType == null) {
            return false;
        }

        List<String> items = contentType.split(';');
        contentType = items.get(0).trim();

        return contentType == 'application/json';
    }


    /**
     * @return True for httpMethods with body
     */
    public Boolean hasBody()
    {
        return this.httpMethod == 'PATCH' || this.httpMethod == 'POST' || this.httpMethod == 'PUT';
    }
}