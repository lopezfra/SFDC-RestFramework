/**
 * Registers the ARC_WebserviceAdapter class based on service name and service version
 *
 * TODO Move routing information to new dispatcher object
 */
public interface ARC_WebserviceAdapterRegistry
{
    /**
     * Registers a route within the registry
     *
     * @param requestPath Path to match route for
     * @param version Version to identify route for
     * @param httpMethod HttpMethod this route is available for
     * @param adapterType Adapter class
     */
    void addRoute (String requestPath, String version, String httpMethod, System.Type adapterType);


    /**
     * Returns the route of a matching ARC_WebserviceAdapter registration
     *
     * @param requestPath Path to find adapter for
     * @param version Webservice version e.g. 'v1'
     * @param httpMethod HttpMethod to find data for
     * @return Found ARC_WebserviceRoute if found; false if not
     */
    ARC_WebserviceRoute getRoute (String requestPath, String version, String httpMethod);
}