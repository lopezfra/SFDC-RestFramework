/**
 * DTO class for exceptions which occur whithin the REST-Framework
 */
public with sharing class ARC_DTO_WebserviceError
{
    public String errorCode { get; private set; }
    public String errorMessage { get; private set; }

    public ARC_DTO_WebserviceError(String errorCode, String errorMessage)
    {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

}