/**
 * Webservice adapter for checking if the webservice framework is accessible at all
 */
public class ARC_WSA_PingV1GET implements ARC_WebserviceAdapter
{
    /**
     * DTO class for the REST request payload
     */
    public class ARC_DTO_Ping
    {
        public String name { get; set; }
    }

    /**
     * Returns ARC_DTO_Ping.class as payload type
     */
    public System.Type getRequestPayloadType()
    {
        return ARC_DTO_Ping.class;
    }


    /**
     * Method for processing HTTP GET calls.
     * Writes { "message" : "Hello world!" } into the response body
     *
     * @param request - Instance of ARC_WebserviceRequest (created based on RestContext.RestRequest)
     * @param response - Instance of ARC_WebserviceResponse (created based on RestContext.RestResponse)
     */
    public void execute (ARC_WebserviceRequest request, ARC_WebserviceResponse response, Object payload)
    {
        response.bodyObject = new Map<String, String>{
            'message' => 'Hello world!'
        };
    }
}