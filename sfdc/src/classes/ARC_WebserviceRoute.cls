/**
 * Route representing a webservice adapter, path and version
 */
public interface ARC_WebserviceRoute
{
    /**
     * @return Instantiated adapter
     */
    ARC_WebserviceAdapter getAdapter ();


    /**
     * @param adapter Instantiated adapter
     */
    void setAdapter (ARC_WebserviceAdapter adapter);


    /**
     * @return WebserviceAdapter of this route
     */
    System.Type getAdapterType ();


    /**
     * @return HttpMethod for this route
     */
    String getHttpMethod ();


    /**
     * @return Route version identifier
     */
    String getVersion ();


    /**
     * @return Request path for this route
     */
    String getPath ();


    /**
     * @return List of bound path variables
     */
    Map<String, String> getPathVariables ();
}