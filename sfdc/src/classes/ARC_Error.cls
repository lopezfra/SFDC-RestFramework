/**
 * This is a static object providing static data for the error handling
 */
public with sharing class ARC_Error {
    public Enum Scope
    {
        /**
         * General application issues e.g. missing data
         */
        Application,

        /**
         * Errors related to SFDC underlying issues
         */
        BaseSystem,

        /**
         * Self explanatory
         */
        Caching,

        /**
         * Everything related to the order process (simulation, submission)
         */
        Order,

        /**
         * Pricing - is that still valid?
         */
        Pricing,

        /**
         * Listing and Exclusion errors
         */
        LE,

        /**
         * Everything relating to security
         */
        Security,

        /**
         * Everything relating to Jobs
         */
        Jobs,

        /**
         * REST WebService related
         */
        REST,

        /**
         * And all the rest
         */
        Misc
    }

    public Enum Code {
        // ------------------ BaseSystem -----------------------------------------------------------------------------
        E0000,          // generic Exception
        E0200,          // generic DMLException
        E0300,          // generic QueryException

        // ------------------ Application ------------------------------------------------------------------------------
        // -------- General
        // -------- i18n/l18n errors
        E1201,           // error retrieving the page label translation via CCRZ ccI18nApi
        // -------- Data errors
        E1301,          // No default ship-to found

        // ------------------ Security ---------------------------------------------------------------------------------
        // -------- Custom Permission Check
        E2101,          // custom Permnission Check globally
        E2102,          // missing custom permission when creating users for a contact

        // ------------------ Order ------------------------------------------------------------------------------------
        // -------- Order submission and simulation
        E3000,           // generic order submission error
        E3010,           // Error setting order comment on order from cart
        E3100,           // generic order simulation error
        E3200,           // generic split order place job error

        // ------------------ Pricing ----------------------------------------------------------------------------------
        // -------- General pricing
        E4000,

        // ------------------ LE ---------------------------------------------------------------------------------------
        // -------- Salesforce Data Extraction
        E5000,           // Generic error when trying to extract data from Salesforce
        // -------- Salesforce Data Load
        E5100,           // Generic error when trying to deliver data to Salesforce
        // -------- Scheduler
        E5200,           // Error when trying to start Clean-Up. There is another scheduled process running.
        // Race condition.
        E5201,           // Error when trying to start Clean-Up. There were errors previous calculation process.
        // Not safe status.
        E5202,           // Error when executing Clean-Up. APEX code can't be executed remotely.
        // -------- Service
        E5300,           // File Service. Unable to archive file.
        E5301,           // File Service. Unable to remove folder.

        // ------------------ Jobs ----------------------------------------------------------------------------------
        // -------- Inventory Clean Job
        E6000,

        // ------------------ Webservice -------------------------------------------------------------------------------
        // -------- Custom webservice errors
        E6100,          // General exception within the webservice framework
        E6101,          // No adapterfactory registered
        E6102,          // Adapter class not found
        E6103,          // No response defined

        // ------------------ Cart -------------------------------------------------------------------------------
        // -------- Cart Service Errors
        E7000,          // Generic cart exception
        E7100,          // Remove cart items exception

        // ------------------ Misc-- -----------------------------------------------------------------------------------
        // -------- Cache
        E9999
    }
}