@isTest
private class ARC_RestServiceControllerTest
{
    private static RestRequest request;

    private static fflib_ApexMocks mocks;

    private static ARC_WebserviceAdapterFactory factory;

    private static ARC_WebserviceRoute route;

    private static ARC_WebserviceAdapter adapter;

    private static ARC_WebserviceAdapterRegistry registry;

    private static ARC_Service_Error errorService;

    private static ARC_RestServiceController controller;


    private static void setUp ()
    {
        request = new RestRequest();
        request.addHeader('X-Test', 'Value');
        request.addParameter('parameter1', 'value1');
        request.addHeader('Content-Type', 'application/json');
        request.requestURI = '/click/v1/path/segment';
        request.resourcePath = '/services/apexrest/click/*';
        request.requestBody = Blob.valueOf('{ "property" : "value" }');
        request.httpMethod = 'POST';

        mocks = new fflib_ApexMocks();
        factory = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapterFactory(mocks);
        route = new ARC_Mock_Webservice.ARC_Mock_WebserviceRoute(mocks);
        adapter = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapter(mocks);
        registry = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapterRegistry(mocks);
        errorService = new ARC_Mock_Service.ARC_Mock_Service_Error(mocks);

        mocks.startStubbing();
        mocks.when(route.getAdapter()).thenReturn(adapter);
        mocks.when(route.getPath()).thenReturn('/path/segment');
        mocks.when(adapter.getRequestPayloadType()).thenReturn(RequestPayloadType.class);
        mocks.stopStubbing();
    }


    @isTest
    private static void testCreateWebserviceRequestFromRestRequest ()
    {
        setUp();

        RestResponse response = new RestResponse();

        Test.startTest();
        controller = new ARC_RestServiceController(request, response, factory, errorService);
        controller.execute();
        Test.stopTest();

        System.assertEquals(new Map<String, String>{
            'parameter1' => 'value1'
        },
            controller.webserviceRequest.urlParameters
        );
        System.assertEquals(new Map<String, String>{
            'Content-Type' => 'application/json',
            'X-Test' => 'Value'
        },
            controller.webserviceRequest.headers
        );
        System.assertEquals('/path/segment', controller.webserviceRequest.requestUri);
        System.assertEquals(request.requestBody, controller.webserviceRequest.requestBody);
        System.assertEquals(request.httpMethod, controller.webserviceRequest.httpMethod);
    }


    @isTest
    private static void testAdapterNotFound ()
    {
        setUp();
        request.requestURI = '/click/v3/service';
        RestResponse response = new RestResponse();

        Test.startTest();
        controller = new ARC_RestServiceController(request, response, factory, errorService);
        controller.execute();
        Test.stopTest();

        System.assertEquals(404, controller.webserviceResponse.statusCode);
    }


    @isTest
    private static void testCreateWebserviceResponseFromRestResponse ()
    {
        setUp();

        request.httpMethod = 'GET';

        expectRouteToReturnPathVariables(new Map<String, String>());

        mocks.startStubbing();
        mocks.when(factory.getRoute('/path/segment', 'v1', 'GET')).thenReturn(route);
        mocks.stopStubbing();

        RestResponse response = new RestResponse();

        Test.startTest();
        controller = new ARC_RestServiceController(request, response, factory, errorService);
        controller.execute();
        Test.stopTest();

        System.assertEquals(200, controller.webserviceResponse.statusCode);
        System.assert(controller.webserviceResponse.headers.containsKey('Webservice-Duration'));
        System.assertEquals('application/json', controller.webserviceResponse.headers.get('Content-Type'));
        System.assertEquals(null, controller.webserviceResponse.bodyObject);
        System.assertEquals('no-cache,s-maxage=0', response.headers.get('Cache-Control'));
        System.assertEquals('-1', response.headers.get('Expires'));
    }


    @isTest
    private static void testExecuteWithoutPayload ()
    {
        setUp();

        request.httpMethod = 'GET';

        expectRouteToReturnPathVariables(new Map<String, String>());

        mocks.startStubbing();
        mocks.when(factory.getRoute('/path/segment', 'v1', 'GET')).thenReturn(route);
        mocks.stopStubbing();

        RestResponse response = new RestResponse();

        Test.startTest();
        controller = new ARC_RestServiceController(request, response, factory, errorService);
        controller.execute();
        Test.stopTest();

        ((ARC_WebserviceAdapter) mocks.verify(adapter)).execute(
            controller.webserviceRequest,
            controller.webserviceResponse,
            null
        );
    }


    @isTest
    private static void testExecuteWithPayload ()
    {
        setUp();

        expectRouteToReturnPathVariables(new Map<String, String>());

        mocks.startStubbing();
        mocks.when(factory.getRoute('/path/segment', 'v1', 'POST')).thenReturn(route);
        mocks.stopStubbing();

        RestResponse response = new RestResponse();

        Test.startTest();
        controller = new ARC_RestServiceController(request, response, factory, errorService);
        controller.execute();
        Test.stopTest();

        ((ARC_WebserviceAdapter) mocks.verify(adapter)).execute(
            controller.webserviceRequest,
            controller.webserviceResponse,
            controller.webservicePayload
        );
    }


    private static void expectRouteToReturnPathVariables (Map<String, String> pathVariables)
    {
        mocks.startStubbing();
        mocks.when(route.getPathVariables()).thenReturn(pathVariables);
        mocks.stopStubbing();
    }


    private class RequestPayloadType
    {
        private String property;
    }


    private class WSAdapterTransactionException extends Exception
    {
    }


    public class WSAdapterTransactionRollback implements ARC_WebserviceAdapter
    {
        public System.Type getRequestPayloadType ()
        {
            return RequestPayloadType.class;
        }

        public void execute (ARC_WebserviceRequest wsReqeust, ARC_WebserviceResponse wsResponse, Object payload)
        {
            Account account = new Account(Name = 'AccountName');
            insert account;

            throw new WSAdapterTransactionException();
        }
    }
}