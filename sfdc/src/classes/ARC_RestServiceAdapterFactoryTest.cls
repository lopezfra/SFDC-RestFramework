@isTest
private class ARC_RestServiceAdapterFactoryTest
{
    private static fflib_ApexMocks mocks;

    private static ARC_WebserviceRoute route;

    private static ARC_WebserviceAdapterRegistry registry;

    private static ARC_WebserviceAdapterFactory adapterFactory;


    private static void setUp ()
    {
        mocks = new fflib_ApexMocks();

        route = new ARC_Mock_Webservice.ARC_Mock_WebserviceRoute(mocks);

        registry = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapterRegistry(mocks);
        adapterFactory = new ARC_RestServiceAdapterFactory(registry);

        mocks.startStubbing();
        mocks.when(registry.getRoute('/service', 'v1', 'GET')).thenReturn(route);
        mocks.when(registry.getRoute('/service', 'v2', 'GET')).thenReturn(null);
        mocks.stopStubbing();
    }


    @isTest
    private static void testGetRoute ()
    {
        setUp();

        mocks.startStubbing();
        mocks.when(route.getAdapterType()).thenReturn(TestAdapter.class);
        mocks.stopStubbing();

        Test.startTest();

        ARC_WebserviceRoute foundRoute = adapterFactory.getRoute('/service', 'v1', 'GET');

        Test.stopTest();

        System.assertEquals(foundRoute, route);
    }


    @isTest
    private static void testGetRouteRetursNullIfRouteNotRegistered ()
    {
        setUp();

        Test.startTest();

        ARC_WebserviceRoute foundRoute = adapterFactory.getRoute('/service', 'v2', 'GET');

        Test.stopTest();

        System.assertEquals(null, foundRoute);
    }


    private class TestAdapter implements ARC_WebserviceAdapter
    {
        public void execute (ARC_WebserviceRequest request, ARC_WebserviceResponse response, Object payload)
        {}


        public System.Type getRequestPayloadType ()
        {
            return null;
        }
    }
}