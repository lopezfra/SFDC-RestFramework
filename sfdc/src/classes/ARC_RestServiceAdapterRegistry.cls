/**
 * Registers the ARC_WebserviceAdapter class based on service name and service version
 */
public class ARC_RestServiceAdapterRegistry implements ARC_WebserviceAdapterRegistry
{
    /**
     * RouteRegex to method-version keyed route
     */
    private final Map<String, Map<String, ARC_WebserviceRoute>> routes = new Map<String, Map<String, ARC_WebserviceRoute>>();


    public void addRoute (String requestPath, String version, String httpMethod, System.Type adapterType)
    {
        ARC_WebserviceRoute route = new ARC_RestWebServiceRoute(adapterType, version, requestPath, httpMethod);

        String regexPath = this.getPathRegex(route);

        if (!this.routes.containsKey(regexPath)) {
            this.routes.put(regexPath, new Map<String, ARC_WebserviceRoute>());
        }

        this.routes.get(regexPath).put(httpMethod + '-' + version, route);
    }


    private String getPathRegex (ARC_WebserviceRoute route)
    {
        String regexPath = route.getPath();
        for (String pathVariable : route.getPathVariables().keySet()) {
            regexPath = regexPath.replace(':' + pathVariable, '([^/]+)');
        }

        return regexPath;
    }



    public ARC_WebserviceRoute getRoute (String requestPath, String version, String httpMethod)
    {
        for (String regexPath : this.routes.keySet()) {

            Pattern pathPattern = Pattern.compile(regexPath);
            Matcher pathMatcher = pathPattern.matcher(requestPath);

            if (pathMatcher.matches()) {

                ARC_WebserviceRoute route = this.routes.get(regexPath).get(httpMethod + '-' + version);

                // no route for HTTP method/version
                if (route == null) {
                    return null;
                }

                Map<String, String> pathVariables = route.getPathVariables();
                List<String> pathVariableNames = new List<String>(pathVariables.keySet());

                for (Integer i = 1; i <= pathMatcher.groupCount(); i++) {

                    // lists index with 0, regex group matches 0 contains the whole string
                    pathVariables.put(pathVariableNames.get(i - 1), pathMatcher.group(i));
                }

                return route;
            }

        }

        // no route found for path matching
        return null;
    }
}