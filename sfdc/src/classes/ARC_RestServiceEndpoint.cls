/**
 * Endpoint class for click related REST calls
 */
@RestResource(urlMapping='/click/*')
global with sharing class ARC_RestServiceEndpoint
{
    private static ARC_WebserviceAdapterRegistry registry = new ARC_RestServiceAdapterRegistry();

    private static Boolean isRegistered = false;


    private static ARC_WebserviceController controller = new ARC_RestServiceController(
        RestContext.request,
        RestContext.response,
        new ARC_RestServiceAdapterFactory(registry),
        ARC_Dependencies.container.getErrorService()
    );


    public ARC_RestServiceEndpoint (
        ARC_WebserviceController injectedController,
        ARC_WebserviceAdapterRegistry injectedRegistry
    )
    {
        controller = injectedController;
        registry = injectedRegistry;
    }


    /**
     * As of SFDC's brilliant implementation of annotations, one method can but inofficially must not contain multiple
     * HTTP method annotations. Therefore, another lovely duplication.
     */
    @HttpDelete
    global static void executeDelete ()
    {
        executeRequest();
    }


    @HttpGet
    global static void executeGet ()
    {
        executeRequest();
    }


    @HttpPatch
    global static void executePatch ()
    {
        executeRequest();
    }


    @HttpPost
    global static void executePost ()
    {
        executeRequest();
    }


    @HttpPut
    global static void executePut ()
    {
        executeRequest();
    }


    private static void executeRequest ()
    {
        registerEndpoints();
        controller.execute();
    }


    private static void registerEndpoints ()
    {
        // one time registration only
        if (isRegistered == true) {
            return;
        }

        //--------------------------------- new paths ------------------------------------------------------------------
        // catalogue
        // registry.addRoute('/:brand/:seasonType/catalog/:pageIndex/:pageSize', 'v1', 'GET', ARC_WSA_CatalogueV1GET.class);
        // registry.addRoute('/:brand/:seasonType/catalog/filter', 'v1', 'GET', ARC_WSA_CatalogueFilterV1GET.class);
        // registry.addRoute('/:brand/:seasonType/catalog/filter/values/:filterEntity/:filterId', 'v1', 'GET', ARC_WSA_CatalogueFilterValuesV1GET.class);

        // cart
        // registry.addRoute('/:brand/:seasonType/cart/active', 'v1', 'GET', ARC_WSA_CartV1GET.class);
        // registry.addRoute('/:brand/:seasonType/cart', 'v1', 'POST', ARC_WSA_CartV1POST.class);
        registry.addRoute('/cart/:encryptedId/sizes/:sku', 'v1', 'GET', ARC_WSA_CartSizesV1GET.class);
        registry.addRoute('/cart/:encryptedId/sizes', 'v1', 'POST', ARC_WSA_CartSizesV1POST.class);

        registry.addRoute('/cart/:encryptedId/add', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);

        // product details
        // registry.addRoute('/:brand/:seasonType/product/:sku', 'v1', 'GET', ARC_WSA_ProductDetailsV1GET.class);


        // session scope user configuration
        registry.addRoute('/storefronts', 'v1', 'GET', ARC_WSA_StorefrontV1GET.class);
        registry.addRoute('/userconfig', 'v1', 'GET', ARC_WSA_UserConfigV1GET.class);
        registry.addRoute('/userconfig', 'v1', 'POST', ARC_WSA_UserConfigV1PATCH.class);

        // wishlist
        // registry.addRoute('/:brand/:seasonType/wishlists', 'v1', 'GET', ARC_WSA_WishlistV1GET.class);
        // registry.addRoute('/:brand/:seasonType/wishlist', 'v1', 'POST', ARC_WSA_WishlistV1POST.class);
        registry.addRoute('/wishlist/:encryptedId/add', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);


        // status endpoints
        registry.addRoute('/ping', 'v1', 'GET', ARC_WSA_PingV1GET.class);


        //--------------------------------- old paths ------------------------------------------------------------------
        // catalogue
        registry.addRoute('/catalog/:brand/:seasonType/:pageIndex/:pageSize', 'v1', 'GET', ARC_WSA_CatalogueV1GET.class);
        registry.addRoute('/catalog/filter/:brand/:seasonType', 'v1', 'GET', ARC_WSA_CatalogueFilterV1GET.class);
        registry.addRoute('/catalog/filter/values/:brand/:seasonType/:filterEntity/:filterId', 'v1', 'GET', ARC_WSA_CatalogueFilterValuesV1GET.class);

        // cart
        registry.addRoute('/cart/active/:brand/:seasonType', 'v1', 'GET', ARC_WSA_CartV1GET.class);
        registry.addRoute('/cart/:brand/:seasonType', 'v1', 'POST', ARC_WSA_CartV1POST.class);
        // registry.addRoute('/cart/add/:encryptedId', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);

        // packages
        registry.addRoute('/packages/:brand/:seasonType', 'v1', 'GET', ARC_WSA_PackagesV1GET.class);


        // product details
        registry.addRoute('/product/:brand/:seasonType/:sku', 'v1', 'GET', ARC_WSA_ProductDetailsV1GET.class);
        registry.addRoute('/product/:brand/:seasonType/:sku/alternatives', 'v1', 'GET', ARC_WSA_ProductAlternativesV1GET.class);
        registry.addRoute('/product/:brand/:seasonType/:sku/sizes', 'v1', 'GET', ARC_WSA_ProductSizesV1GET.class);

        // wishlist
        registry.addRoute('/wishlists/:brand/:seasonType', 'v1', 'GET', ARC_WSA_WishlistV1GET.class);
        registry.addRoute('/wishlist/:brand/:seasonType', 'v1', 'POST', ARC_WSA_WishlistV1POST.class);
        // registry.addRoute('/wishlist/add/:encryptedId', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);
    }
}