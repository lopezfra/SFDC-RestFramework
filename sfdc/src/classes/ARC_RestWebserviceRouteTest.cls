@IsTest
private with sharing class ARC_RestWebserviceRouteTest
{
    private static ARC_RestWebServiceRoute route;


    @IsTest
    private static void testInstantiationSetsAdapterType ()
    {
        System.Type adapterType = ARC_Mock_Webservice.ARC_Mock_WebserviceAdapter.class;

        route = new ARC_RestWebserviceRoute(adapterType, null, '', null);

        System.assertEquals(adapterType, route.getAdapterType());
    }


    @IsTest
    private static void testInstantiationSetsVersion ()
    {
        String version = 'version';

        route = new ARC_RestWebServiceRoute(null, version, '', null);

        System.assertEquals(version, route.getVersion());
    }


    @IsTest
    private static void testInstantiationSetsHttpMethod ()
    {
        String httpMethod = 'httpMethod';

        route = new ARC_RestWebServiceRoute(null, null, '', httpMethod);

        System.assertEquals(httpMethod, route.getHttpMethod());
    }


    @IsTest
    private static void testInstantiationSetsPath ()
    {
        String path = 'path';

        route = new ARC_RestWebServiceRoute(null, null, path, null);

        System.assertEquals(path, route.getPath());
    }


    @IsTest
    private static void testGetSetAdapter ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '', null);

        System.assertEquals(null, route.getAdapter());

        ARC_WebserviceAdapter adapter = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapter(new fflib_ApexMocks());

        route.setAdapter(adapter);

        System.assertEquals(adapter, route.getAdapter());
    }


    @IsTest
    private static void testGetPathVariablesForEmptyPath ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '', null);

        System.assertEquals(new Map<String, String>(), route.getPathVariables());
    }


    @IsTest
    private static void testGetPathVariablesForNoPath ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/', null);

        System.assertEquals(new Map<String, String>(), route.getPathVariables());
    }


    @IsTest
    private static void testGetPathVariablesForOnePath ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/part1', null);

        System.assertEquals(new Map<String, String>(), route.getPathVariables());
    }


    @IsTest
    private static void testGetPathVariablesRegardsAnyCharacter ()
    {
        String variableName = ':^°"§$%&(){}[]=?´`+*äöü-_.;µ<>|';

        route = new ARC_RestWebServiceRoute(null, null, '/:' + variableName, null);

        System.assertEquals(
            new Map<String, String>{
                variableName => null
            },
            route.getPathVariables()
        );
    }


    @IsTest
    private static void testGetPathVariablesForLeadingPathVariable ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/:var1', null);

        System.assertEquals(
            new Map<String, String>{
                'var1' => null
            },
            route.getPathVariables()
        );
    }


    @IsTest
    private static void testGetPathVariablesForTrailingPathVariable ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/part/:var1', null);

        System.assertEquals(
            new Map<String, String>{
                'var1' => null
            },
            route.getPathVariables()
        );
    }


    @IsTest
    private static void testGetPathVariablesForAllPathVariables ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/:var1/:var2', null);

        System.assertEquals(
            new Map<String, String>{
                'var1' => null,
                'var2' => null
            },
            route.getPathVariables()
        );
    }


    @IsTest
    private static void testGetPathVariablesForMixedPathVariables ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/:var1/path/:var2', null);

        System.assertEquals(
            new Map<String, String>{
                'var1' => null,
                'var2' => null
            },
            route.getPathVariables()
        );
    }


    @IsTest
    private static void testGetPathVariablesRegardsOrderOfVariables ()
    {
        route = new ARC_RestWebServiceRoute(null, null, '/:var2/:var1', null);

        System.assertEquals(
            new Map<String, String>{
                'var2' => null,
                'var1' => null
            },
            route.getPathVariables()
        );
    }
}