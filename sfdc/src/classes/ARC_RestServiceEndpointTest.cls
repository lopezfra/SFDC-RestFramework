@isTest
private class ARC_RestServiceEndpointTest
{
    private static fflib_ApexMocks mocks;

    private static ARC_WebserviceController controller;

    private static ARC_WebserviceAdapterRegistry registry;

    private static ARC_RestServiceEndpoint endpoint;


    private static void setUp ()
    {
        mocks = new fflib_ApexMocks();
        controller = new ARC_Mock_Webservice.ARC_Mock_WebserviceController(mocks);
        registry = new ARC_Mock_Webservice.ARC_Mock_WebserviceAdapterRegistry(mocks);

        endpoint = new ARC_RestServiceEndpoint(
            controller,
            registry
        );
    }


    @isTest
    private static void testExecuteDelete ()
    {
        setUp();

        Test.startTest();
        ARC_RestServiceEndpoint.executeDelete();
        Test.stopTest();

        assertAdapterRegistrations();

        ((ARC_WebserviceController) mocks.verify(controller)).execute();
    }


    @isTest
    private static void testExecuteGet ()
    {
        setUp();

        Test.startTest();
        ARC_RestServiceEndpoint.executeGet();
        Test.stopTest();

        assertAdapterRegistrations();

        ((ARC_WebserviceController) mocks.verify(controller)).execute();
    }


    @isTest
    private static void testExecutePatch ()
    {
        setUp();

        Test.startTest();
        ARC_RestServiceEndpoint.executePatch();
        Test.stopTest();

        assertAdapterRegistrations();

        ((ARC_WebserviceController) mocks.verify(controller)).execute();
    }


    @isTest
    private static void testExecutePost ()
    {
        setUp();

        Test.startTest();
        ARC_RestServiceEndpoint.executePost();
        Test.stopTest();

        assertAdapterRegistrations();

        ((ARC_WebserviceController) mocks.verify(controller)).execute();
    }


    @isTest
    private static void testExecutePut ()
    {
        setUp();

        Test.startTest();
        ARC_RestServiceEndpoint.executePut();
        Test.stopTest();

        assertAdapterRegistrations();

        ((ARC_WebserviceController) mocks.verify(controller)).execute();
    }


    private static void assertAdapterRegistrations ()
    {
        //--------------------------------- new paths ------------------------------------------------------------------
        // catalogue
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/catalog/:pageIndex/:pageSize', 'v1', 'GET', ARC_WSA_CatalogueV1GET.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/catalog/filter', 'v1', 'GET', ARC_WSA_CatalogueFilterV1GET.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/catalog/filter/values/:filterEntity/:filterId', 'v1', 'GET', ARC_WSA_CatalogueFilterValuesV1GET.class);

        // cart
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/cart/active', 'v1', 'GET', ARC_WSA_CartV1GET.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/cart', 'v1', 'POST', ARC_WSA_CartV1POST.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/cart/:encryptedId/add', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);

        // session scope user configuration
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/storefronts', 'v1', 'GET', ARC_WSA_StorefrontV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/userconfig', 'v1', 'GET', ARC_WSA_UserConfigV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/userconfig', 'v1', 'POST', ARC_WSA_UserConfigV1PATCH.class);

        // wishlist
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/wishlists', 'v1', 'GET', ARC_WSA_WishlistV1GET.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/:brand/:seasonType/wishlist', 'v1', 'POST', ARC_WSA_WishlistV1POST.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/wishlist/:encryptedId/add', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);


        // status endpoints
        ((ARC_WebserviceAdapterRegistry)mocks
            .verify(registry)).addRoute('/ping', 'v1', 'GET', ARC_WSA_PingV1GET.class);


        //--------------------------------- old paths ------------------------------------------------------------------
        // catalogue
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/catalog/:brand/:seasonType/:pageIndex/:pageSize', 'v1', 'GET', ARC_WSA_CatalogueV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/catalog/filter/:brand/:seasonType', 'v1', 'GET', ARC_WSA_CatalogueFilterV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/catalog/filter/values/:brand/:seasonType/:filterEntity/:filterId', 'v1', 'GET', ARC_WSA_CatalogueFilterValuesV1GET.class);

        // cart
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/cart/active/:brand/:seasonType', 'v1', 'GET', ARC_WSA_CartV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/cart/:brand/:seasonType', 'v1', 'POST', ARC_WSA_CartV1POST.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/cart/add/:encryptedId', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);

        // wishlist
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/wishlists/:brand/:seasonType', 'v1', 'GET', ARC_WSA_WishlistV1GET.class);
        ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
            .addRoute('/wishlist/:brand/:seasonType', 'v1', 'POST', ARC_WSA_WishlistV1POST.class);
        // ((ARC_WebserviceAdapterRegistry)mocks.verify(registry))
        //     .addRoute('/wishlist/add/:encryptedId', 'v1', 'POST', ARC_WSA_CartAddV1POST.class);
    }
}