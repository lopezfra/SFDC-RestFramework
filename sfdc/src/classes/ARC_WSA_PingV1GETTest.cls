@isTest
private class ARC_WSA_PingV1GETTest
{
    private static Object payload;
    private static ARC_WSA_PingV1GET adapter;

    private static void setup()
    {
        ARC_WSA_PingV1GET.ARC_DTO_Ping pingPayload = new ARC_WSA_PingV1GET.ARC_DTO_Ping();
        pingPayload.name = 'user';

        payload = (object) pingPayload;
        adapter = new ARC_WSA_PingV1GET();
    }

    @isTest
    private static void testExecute ()
    {
        setup();
        ARC_WebserviceResponse response = new ARC_WebserviceResponse();

        Test.startTest();
        adapter.execute(null, response, null);
        Test.stopTest();

        System.assertEquals('Hello world!', ((Map<String, String>)response.bodyObject).get('message'));
    }
}