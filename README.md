# Apex Rest Framework

Creation of an Apex REST-Framework which unifies the creation of webservice logic by using webservice adapter instead of using the standard Salesforce REST-API.

## Requeriments

- Only custom webservices should be used to be able to extend the implemented logic if required.
- Every REST call should be redirected to a specific webservice adapter class which implementes the ARC_WebserviceAdapter interface 
- If an error occurs all database transactions should be rolled back
- If an error occurs the error should be logged away for later analysis

### Solution

To achieve this only one custom webervice, ARC_RestServiceEndpoint, will be created which mapped to /click/*. This means that every CLICK related REST should be handled by this class.
Based on the HTTP method a specific controller function in ARC_RestServiceController will be executed which creates an instance of an adapter classe and executes the logic in it.

![Screen_Shot_2017-09-17_at_01.22.07](/uploads/d0205dc9b59e690fd4698f03b4fb1a7f/Screen_Shot_2017-09-17_at_01.22.07.png)


### Example:  ARC_WSAdapter_PingV1

Withing ARC_RestServiceEndpoint the webservice adapter is registered so that the adapter factory is able to an instance of it within the controller:

```java
private static void registerEndpoints(){
 // registry.addAdapterImplementation(<path>, <version>, <HTTP Method>,<adapter class>);
    registry.addRoute('/ping', 'v1', 'GET', ARC_WSA_PingV1GET.class);
    ... 
}
```

To call the according webservice following URI has to convention has to be used: /click/<version>/<service> So in this specific case the request URI is /click/v1/ping.

This adapter contains logic for HTTP GET and HTTP POST. The GET method, just returns
```java
     {"message":"Hello world!"}
```
The HTTP POST logic with following request body:
```java
     {"name":"Adi Dassler"}
```
Will return
```java
     {"message":"Hello Adi Dassler!"}
```
This service can be used to make sure that the REST-Framework can be used at all.




